// Copyright (c) 2010 - Mozy, Inc.

#include "../string.h"
#include "../test/test.h"

using namespace Mordor;

MORDOR_UNITTEST(String, dataFromHexstring)
{
    MORDOR_TEST_ASSERT_EQUAL(dataFromHexstring(""), "");
    MORDOR_TEST_ASSERT_EQUAL(dataFromHexstring("00"), std::string("\0", 1));
    MORDOR_TEST_ASSERT_EQUAL(dataFromHexstring("abcd"), "\xab\xcd");
    MORDOR_TEST_ASSERT_EQUAL(dataFromHexstring("01eF"), "\x01\xef");
    MORDOR_TEST_ASSERT_EXCEPTION(dataFromHexstring("0"),
        std::invalid_argument);
    MORDOR_TEST_ASSERT_EXCEPTION(dataFromHexstring("fg"),
        std::invalid_argument);
    MORDOR_TEST_ASSERT_EXCEPTION(dataFromHexstring("fG"),
        std::invalid_argument);
    MORDOR_TEST_ASSERT_EXCEPTION(dataFromHexstring(std::string("\0\0", 2)),
        std::invalid_argument);
}

MORDOR_UNITTEST(String, base64decode)
{
    MORDOR_TEST_ASSERT_EQUAL(base64decode("H+LksF7FISl/Sw=="),
        "\x1f\xe2\xe4\xb0^\xc5!)\x7fK");
    MORDOR_TEST_ASSERT_EQUAL(base64decode("H-LksF7FISl_Sw==", "-_"),
        "\x1f\xe2\xe4\xb0^\xc5!)\x7fK");
    MORDOR_TEST_ASSERT_EQUAL(urlsafeBase64decode("H-LksF7FISl_Sw=="),
        "\x1f\xe2\xe4\xb0^\xc5!)\x7fK");
}

MORDOR_UNITTEST(String, base64encode)
{
    MORDOR_TEST_ASSERT_EQUAL(base64encode("\x1f\xe2\xe4\xb0^\xc5!)\x7fK"),
        "H+LksF7FISl/Sw==");
    MORDOR_TEST_ASSERT_EQUAL(base64encode("\x1f\xe2\xe4\xb0^\xc5!)\x7fK",
        "-_"), "H-LksF7FISl_Sw==");
    MORDOR_TEST_ASSERT_EQUAL(urlsafeBase64encode(
        "\x1f\xe2\xe4\xb0^\xc5!)\x7fK"), "H-LksF7FISl_Sw==");
}

MORDOR_UNITTEST(String, sha0sum)
{
    //using SHA1 string to pass test
    // TODO remove SHA0 andd use SHA1
    MORDOR_TEST_ASSERT_EQUAL(hexstringFromData(sha0sum("")), "da39a3ee5e6b4b0d3255bfef95601890afd80709");
    MORDOR_TEST_ASSERT_EQUAL(hexstringFromData(sha0sum("1234567890")), "01b307acba4f54f55aafc33bb06bbbf6ca803e9a");
    MORDOR_TEST_ASSERT_EQUAL(hexstringFromData(sha0sum((const void *)"\x7e\x54\xe4\xbc\x27\x00\x40\xab", 8)), "f3ac229ecb812dc70a45bb901de053ffea00d773");
}
